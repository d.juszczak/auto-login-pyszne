const config = {
    token:'token',
    username: 'username_auto_login',
    password: 'pass_auto_login',
    salt: ''
}

const cipher = salt => {
    const textToChars = text => text.split('').map(c => c.charCodeAt(0));
    const byteHex = n => ("0" + Number(n).toString(16)).substr(-2);
    const applySaltToChar = code => textToChars(salt).reduce((a,b) => a ^ b, code);

    return text => text.split('')
        .map(textToChars)
        .map(applySaltToChar)
        .map(byteHex)
        .join('');
}

const decipher = salt => {
    const textToChars = text => text.split('').map(c => c.charCodeAt(0));
    const applySaltToChar = code => textToChars(salt).reduce((a,b) => a ^ b, code);
    return encoded => encoded.match(/.{1,2}/g)
        .map(hex => parseInt(hex, 16))
        .map(applySaltToChar)
        .map(charCode => String.fromCharCode(charCode))
        .join('');
}

const setToken = (token) =>localStorage.setItem(config.token, token)

setTimeout(()=>{
    const myDecipher = decipher(config.salt)
    const body = JSON.stringify({
        "username": localStorage.getItem(config.username),
        "password": myDecipher(localStorage.getItem(config.password))
    })
    fetch('https://live-orders-api.takeaway.com/api/login',{
        method: 'POST', 
        mode: 'cors', 
        cache: 'no-cache', 
        credentials: 'same-origin', 
        headers: {
          'Content-Type': 'application/json'
        },
        redirect: 'follow', 
        referrerPolicy: 'no-referrer', 
        body
      })
    .then(response=>response.json())
    .then(data => {
        setToken(data.access_token)
        window.location.reload()
    })
},5000)